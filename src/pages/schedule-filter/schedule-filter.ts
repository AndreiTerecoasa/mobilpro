import { Component } from '@angular/core';

import { NavParams, ViewController } from 'ionic-angular';

import { AppsData } from '../../providers/apps-data';


@Component({
  selector: 'page-schedule-filter',
  templateUrl: 'schedule-filter.html'
})
export class ScheduleFilterPage {
  tracks: Array<{name: string, id: number, isChecked: boolean}> = [];

  constructor(
    public appsData: AppsData,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    // passed in array of track names that should be excluded (unchecked)
    let excludedTrackNames = this.navParams.data;

    this.appsData.getTracks().forEach(track => {
      this.tracks.push({
        name: track.name,
        id: track.id,
        isChecked: (excludedTrackNames.indexOf(track.id) === -1)
      });
    });
  }

  resetFilters() {
    // reset all of the toggles to be checked
    this.tracks.forEach(track => {
      track.isChecked = true;
    });
  }

  applyFilters() {
    // Pass back a new array of track names to exclude
    let excludedTrackIds = this.tracks.filter(c => !c.isChecked).map(c => c.id);
    console.log(excludedTrackIds);
    this.dismiss(excludedTrackIds);
  }

  dismiss(data?: any) {
    // using the injected ViewController this page
    // can "dismiss" itself and pass back data
    this.viewCtrl.dismiss(data);
  }
}
