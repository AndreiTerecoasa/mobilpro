import { Component, ViewChild, ElementRef } from '@angular/core';

import { AppsData } from '../../providers/apps-data';

import { Platform,Events } from 'ionic-angular';
import {
	GoogleMap,
	GoogleMapsEvent,
	GoogleMapsLatLng,
	CameraPosition,
	GoogleMapsMarkerOptions,
	GoogleMapsMarker,
	//  GoogleMapsMapTypeId
} from 'ionic-native';

declare var google: any;

@Component({
	selector: 'page-map',
	templateUrl: 'map.html'
})
export class MapPage {
	mapData: any;
	@ViewChild('mapCanvas') mapElement: ElementRef;
	constructor(public appsData: AppsData, public platform: Platform, 	public events: Events) {
	}
	
	
	ngAfterViewInit() {
		this.mapData = this.appsData.getMap();
		this.loadMap();
	}
	loadMap() {
		// create a new map by passing HTMLElement
		let element: HTMLElement = document.getElementById('map_canvas');
		
		let map = new GoogleMap(element);
		
		// create LatLng object
		let center: any = this.mapData.filter(marker => {
			return marker.center == true;
		})
		let ionic: GoogleMapsLatLng = new GoogleMapsLatLng(center[0].lat,center[0].lng);
		// create CameraPosition
		let position: CameraPosition = {
			target: ionic,
			zoom: 15,
			tilt: 15
		};
		
		// listen to MAP_READY event
		map.one(GoogleMapsEvent.MAP_READY).then(() => {
			// move the map's camera to position
			map.moveCamera(position); // works on iOS and Android
			this.mapData.forEach(marker => {
				map.addMarker({
					position: new GoogleMapsLatLng(marker.lat, marker.lng),
					title: marker.name,
					snippet: "Address: " + marker.address
				})
			})
		});
		
		 this.events.subscribe('sidebar:open', () => {
				map.setClickable(false);
			});

			this.events.subscribe('sidebar:close', () => {
				map.setClickable(true)
			});

	}
}
