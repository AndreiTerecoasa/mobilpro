import { Component } from '@angular/core';
import { NavController, AlertController, ToastController } from 'ionic-angular';


@Component({
  selector: 'page-user',
  templateUrl: 'feedback.html'
})
export class FeedbackPage {

  submitted: boolean = false;
  feedbackMessage: string;

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController
  ) {

  }

  ionViewDidEnter() {
    let toast = this.toastCtrl.create({
      message: 'This does not actually send a support request.',
      duration: 3000
    });
    toast.present();
  }

  submit(form) {
    this.submitted = true;

    if (form.valid) {
      this.feedbackMessage = '';
      this.submitted = false;

      let toast = this.toastCtrl.create({
        message: 'Your support request has been sent.',
        duration: 3000
      });
      toast.present();
    }
  }

  // If the user enters text in the support question and then navigates
  // without submitting first, ask if they meant to leave the page
  ionViewCanLeave(): boolean | Promise<boolean> {
    // If the support message is empty we should just navigate
    if (!this.feedbackMessage || this.feedbackMessage.trim().length === 0) {
      return true;
    }

    // return new Promise((resolve: any, reject: any) => {
    //   let alert = this.alertCtrl.create({
    //     title: 'Leave this page?',
    //     message: 'Are you sure you want to leave this page? Your support message will not be submitted.'
    //   });
    //   alert.addButton({ text: 'Stay', handler: reject });
    //   alert.addButton({ text: 'Leave', role: 'cancel', handler: resolve });

    //   alert.present();
    // });
  }
}
