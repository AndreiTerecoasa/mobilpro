import { Component } from '@angular/core';
import { Events } from 'ionic-angular';
import { NavController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { UserData } from '../../providers/user-data';
import { Facebook } from 'ionic-native';

@Component({
    selector: 'page-user',
    templateUrl: 'signup.html'
})
export class SignupPage {
    signup: {
        email?: string, 
        password?: string,
        lastname?: string,
        firstname?: string,
        title?: string,
        scope?: string,
        organisation?: string
    } = {};
    submitted = false;
    registerLoader: any;
    loginLoader: any;

    constructor(
        public loadingCtrl: LoadingController, 
        public navCtrl: NavController, 
        public userData: UserData, 
        public events: Events,) {
            
            this.registerLoaders();
            this.registerEvents();
    }
    registerLoaders() {
        this.registerLoader = this.loadingCtrl.create({
            content: 'Please wait! We are registering you..'
        });
        this.loginLoader = this.loadingCtrl.create({
            content: 'You have been registered. Now you will be loggedin'
        });
    }
    registerEvents() {
        this.events.subscribe('user:registered', (email, password) => {
            this.registerLoader.dismiss();
            this.loginLoader.present();
            this.userData.login(email, password);
        })
        this.events.subscribe('user:loggedin', (data) => {
            console.log(data);
            if(data.type == 1) {
                this.loginLoader.dismiss();
                this.userData.setLoggedIn(data.key);
                this.navCtrl.push(TabsPage);
            }
        })
    }
    onSignup(form) {
        
        
        this.submitted = true;

        if (form.valid) {
            this.userData.signup(this.signup);

            this.registerLoader.present();
            
        }
    }
    facebookSignup() {
        Facebook.login(['public_profile','email']).then(user=> {
            Facebook.api('/'+user.authResponse.userID + '/?fields=name,email', ['public_profile','email']).then(data => {
                    this.signup = {
                        email: data.email,
                        password: this.makePassword(),
                        lastname: data.name.split(" ")[1],
                        firstname: data.name.split(" ")[0],
                        title: '',
                        scope: '',
                    };
                    this.userData.signup(this.signup);
            }).catch(reason => {
                console.log(JSON.stringify(reason));
            })
        })
    }
    makePassword() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < 10; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
    goBack() {
        this.navCtrl.push(TabsPage);
    }
}
