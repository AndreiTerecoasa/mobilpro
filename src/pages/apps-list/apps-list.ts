import { Component } from '@angular/core';

import { ActionSheet, ActionSheetController, Config, NavController, ToastController } from 'ionic-angular';
import { InAppBrowser, SocialSharing  } from 'ionic-native';

import { AppsData } from '../../providers/apps-data';
import { SessionDetailPage } from '../session-detail/session-detail';
import { AppDetailPage } from '../app-detail/app-detail';
import { AppContactPage } from '../app-contact/app-contact';
import { Storage } from '@ionic/storage';

@Component({
    selector: 'page-apps-list',
    templateUrl: 'apps-list.html'
})
export class AppsListPage {
    actionSheet: ActionSheet;
    applications:any = [];
    appsSearch: string = "";

    constructor(
        public actionSheetCtrl: ActionSheetController, 
        public navCtrl: NavController,
        public toastCtrl: ToastController, 
        public appsData: AppsData, 
        public config: Config,
        public storage: Storage) {

        }

    ionViewDidLoad() {
        this.applications = this.appsData.getApps();
    }
    
    appsRefresh(refresher) {
		this.appsData.loadAppsFromApi().then(apps => {
            console.log(apps);
            this.applications = apps;
            refresher.complete();
        })
	}
    
    
    
    
    gotToAppDetail(app: any) {
       this.navCtrl.push(AppDetailPage, app);
        
    } 

    sendMessage(id: any) {
        this.storage.get('authKey').then(value => {
            if(value) {
                this.navCtrl.push(AppContactPage, id);
            } else {
                let toast = this.toastCtrl.create({
                    message: 'You must be logged in to do that',
                    duration: 4000,
                    position: 'bottom'
                });
                toast.present();    
            }
        })
        
        
    }
    goToSessionDetail(session) {
        this.navCtrl.push(SessionDetailPage, session);
    }

    appSearchOnInput($event) {
        console.log('searching app');
        if(this.appsSearch == "") {
            this.applications = this.appsData.getApps();
        } else {
            this.applications = this.applications.filter(obj => {
                return (obj.name.toLowerCase().indexOf(this.appsSearch.toLowerCase()) > -1);
            })
        }
        
    }
    appSearchOnCancel($event) {
        this.applications = this.appsData.getApps();
    }

    goToSpeakerTwitter(speaker) {
        new InAppBrowser(`https://twitter.com/${speaker.twitter}`, '_blank');
    }

    openAppShare(app) {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Share ' + app.name,
            buttons: [
            {
                text: 'Share via ...',
                handler: ($event) => {
                    SocialSharing.share('Test', 'test', null, 'https://mpvs.lse.org.ro/application/' + app.slug).then(value => {
                        console.log("wtf");
                    }).catch(value => {
                        console.log("wtf2");
                    })
                }
            },
            {
                text: 'Cancel',
                role: 'cancel'
            }
            ]
        });

        actionSheet.present();
    }

    openContact(speaker) {
        let mode = this.config.get('mode');

        let actionSheet = this.actionSheetCtrl.create({
            title: 'Contact ' + speaker.name,
            buttons: [
            {
                text: `Email ( ${speaker.email} )`,
                icon: mode !== 'ios' ? 'mail' : null,
                handler: () => {
                    window.open('mailto:' + speaker.email);
                }
            },
            {
                text: `Call ( ${speaker.phone} )`,
                icon: mode !== 'ios' ? 'call' : null,
                handler: () => {
                    window.open('tel:' + speaker.phone);
                }
            }
            ]
        });

        actionSheet.present();
    }
}
