import {
    Component
} from '@angular/core';

import {
    AlertController,
    NavController,
	Platform
} from 'ionic-angular';

import {
    LoginPage
} from '../login/login';
import {
    FeedbackPage
} from '../feedback/feedback';
import {
    UserData
} from '../../providers/user-data';
import { Push } from 'ionic-native';

@Component({
    selector: 'page-account',
    templateUrl: 'account.html'
})
export class AccountPage {
    authKey: string = '';
	push: any;
	isRegisteredNotifyable: boolean = false;
    pushRegistrationId: any = '';
    userD: {
        id ? : number,
        name ? : string,
        mail ? : string,
        scope ? : string,
        title ? : string
    } = {
        id: -1,
        name: '',
        mail: '',
        scope: '',
        title: ''
    };
    editMode: boolean = false;

    constructor(
		public alertCtrl: AlertController, 
		public nav: NavController, 
		public userData: UserData,
		public platform: Platform) {
            this.platform.ready().then(() => {
                console.log('platform ready');
                this.push = Push.init({
                    android: {
                        senderID: "186613180055"
                    },
                    ios: {
                        alert: "true",
                        badge: false,
                        sound: "true"
                    },
                    windows: {}
                });
                this.push.on('registration', (data) => {
                    console.log(data);
                    if(data.registrantionId != '') {
                         this.pushRegistrationId = data.registrationId;
                    }
                   
                });
            })
    }

    ionViewWillEnter() {
        if (this.authKey == '') {
            console.log("getting key");
            this.getAuthKey();
        } else {
            console.log("have key");
        }
        this.editMode = false;
        this.userData.getUserData().then(user => {
            console.log(user);
            this.userD = user;
        })

        this.checkRegisteredNotifyable();
    }
    ionViewDidEnter() {


    }


    getAuthKey() {
        this.userData.getAuthKey().then(authKey => this.authKey = authKey);
    }

    changeData() {
        this.editMode = true;
    }
    saveData() {
        this.editMode = false;
    }

    subscribeToNotif() {
        let subscribeToNotif = this.alertCtrl.create({
            title: 'Subscribe to notifications?',
            message: 'You will receive push notifications about event updates and other news',
            buttons: [{
                    text: 'Ok',
                    role: 'Ok',
                    handler: () => {
                        if(this.pushRegistrationId != '') {
                            this.userData.registerNotifyable(this.pushRegistrationId).then(response => {
                                console.log(JSON.stringify(response));
                                this.checkRegisteredNotifyable();
                            })
                        }
                        
						
                    }
                },
                {
                    text: 'No',
                    role: 'No',
                    handler: () => {
                        
                    }
                }
            ]
        });

        if(this.isRegisteredNotifyable) {

        } else {
            subscribeToNotif.present();
        }

    }
    unsubscribeToNotif() {
        this.userData.unregisterNotifyable().then(data => {
            this.alertCtrl.create({
                title: 'Notifications',
                message: '' + data,
                buttons: []
            }).present();
        })
    }
    changePassword() {
        let alert = this.alertCtrl.create({
            title: 'Change Password',
            buttons: [
                'Cancel'
            ]
        });
        alert.addInput({
            name: 'password',
            value: '',
            placeholder: 'password'
        });
        alert.addButton({
            text: 'Ok',
            handler: data => {
                this.userData.changePassword(data.password).then(data => {
                    console.log(data);
                });

            }
        });

        alert.present();
    }

    logout() {
        this.userData.logout().then(value => {
            this.alertCtrl.create({
                title: 'Logged Out',
                message: 'You have been logged out',
                buttons: [
                    {
                        text: 'Ok'
                    }
                ]
            }).present();
            this.nav.setRoot(LoginPage);
        })
        
    }

    support() {
        this.nav.push(FeedbackPage);
    }
    checkRegisteredNotifyable() {
        this.userData.isRegisteredNotifyable().then(is => {
			this.isRegisteredNotifyable = is;
		})
    }
}
