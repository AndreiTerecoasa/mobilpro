import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import { SessionDetailPage } from '../session-detail/session-detail';


@Component({
  selector: 'page-app-detail',
  templateUrl: 'app-detail.html'
})
export class AppDetailPage {
  app: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.app = this.navParams.data;
    console.log(this.app);
  }

}
