import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { AppsData } from '../../providers/apps-data';
import { AppsListPage } from '../apps-list/apps-list';

/*
  Generated class for the AppContact page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-app-contact',
  templateUrl: 'app-contact.html'
})
export class AppContactPage {
  id: any;
  message: string = '';
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public appsData: AppsData,
    public alertCtrl: AlertController) {
      this.id = this.navParams.data;
    }
  sendMessage() {
    if(this.message == "") return;
    this.appsData.appSendMessage(this.id, this.message).then((data) => {
      if(data == 1) {
         let alert = this.alertCtrl.create({
          title: 'Message Sent',
          subTitle: 'Your message was sent',
          buttons: [
            {
              text: 'Ok',
              role: 'done',
              handler: () => {
                this.navCtrl.pop()
              }
            }
          ]
        });
        alert.present();
      }
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AppContactPage');
  }

}
