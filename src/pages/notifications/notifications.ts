import {
    Component
} from '@angular/core';
import {
    NavController,
    NavParams
} from 'ionic-angular';
import {
    UserData
} from '../../providers/user-data';
@Component({
    selector: 'page-notifications',
    templateUrl: 'notifications.html'
})
export class NotificationsPage {
    isRegisteredNotifyable: boolean = false;
    isLoggedIn: boolean = false;
    notifications: any[] = [];
    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public userData: UserData
    ) {

    }	

    ionViewDidLoad() {
        this.checkLoggedIn();
		this.checkRegisteredNotifyable();
        this.getNotifications();
    }

	checkRegisteredNotifyable() {
        if(this.isLoggedIn) {
            this.userData.isRegisteredNotifyable().then(is => {
                this.isRegisteredNotifyable = is;
            })
        }
    }
    
    checkLoggedIn() {
        this.userData.hasLoggedIn().then(is => {
            this.isLoggedIn = is;
            this.checkRegisteredNotifyable();
        })
    }

    registerNotifyable() {

    }

    getNotifications() {
        this.userData.getNotifications().then(notifications => {
            this.notifications = notifications["notifications"];
        })
    }

}
