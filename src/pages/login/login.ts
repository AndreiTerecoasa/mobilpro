import {
    Component
} from '@angular/core';

import {
    NavController,
    AlertController,
    LoadingController
} from 'ionic-angular';
import {
    Events
} from 'ionic-angular';
import {
    SignupPage
} from '../signup/signup';
import {
    TabsPage
} from '../tabs/tabs';
import {
    UserData
} from '../../providers/user-data';
import {
    Facebook
} from 'ionic-native';

@Component({
    selector: 'page-user',
    templateUrl: 'login.html'
})
export class LoginPage {
    login: {
        email ? : string,
        password ? : string
    } = {};
    submitted = false;
    alertLoginCorrect: any;
    alertLoginIncorrect: any;
    constructor(
        public events: Events,
        public navCtrl: NavController,
        public userData: UserData,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController) {

        this.registerEvents();


        this.alertLoginCorrect = this.alertCtrl.create({
            title: 'Logged In',
            subTitle: 'You have been logged in. You will be taken to the main page.',
            buttons: [{
                text: 'Ok',
                handler: () => {
                    this.navCtrl.push(TabsPage);
                }
            }]
        });
    }
    registerEvents() {
        
        this.events.subscribe('user:nosuchuser', () => {
            this.alertCtrl.create({
                title: 'No such user',
                message: 'There is no account associated with this Facebook account. Please first register',
                buttons: [{
                        text: 'Register',
                        role: 'gotoregister',
                        handler: () => {
                            this.navCtrl.push(SignupPage);
                        }
                    },
                    {
                        text: 'Cancel',
                        role: 'cancel'
                    }
                ]
            }).present();
        })
    }
    facebookLogin() {
        Facebook.login(['public_profile', 'email']).then(user => {
            console.log(JSON.stringify(user));
            Facebook.api('/' + user.authResponse.userID + '/?fields=name,email', ['public_profile', 'email']).then(data => {
                console.log(JSON.stringify(data));
                this.userData.loginFb(data.email, user.authResponse.accessToken);
            }).catch(reason => {
                console.log(JSON.stringify(reason));
            })
        })
    }


    onLogin(form) {
        this.submitted = true;

        if (form.valid) {
            this.userData.login(this.login.email, this.login.password);
            let loadingLogin = this.loadingCtrl.create({
                spinner: 'dots',
                content: 'You are being logged in. Please Wait.'
            })
            loadingLogin.present();
			this.events.subscribe('user:loggedin', (data) => {
				loadingLogin.dismiss();
				console.log(data);
				if (data.type == 1) {
					this.userData.setLoggedIn(data);
					this.alertLoginCorrect.present();
				} else {
					loadingLogin.dismiss();
					this.alertLoginIncorrect.present();
				}
			})
        }
    }


    resetPassword() {
        let resetPasswordPrompt = this.alertCtrl.create({
            title: 'Reset Password',
            message: 'Enter a new password',
            inputs: [{
                name: 'email',
                placeholder: 'Type your email address',
            }],
            buttons: [{
                    text: 'Cancel',
                    handler: data => {

                    }
                },
                {
                    text: 'Change',
                    handler: data => {
                        if (data.email !== "") {
                            this.userData.resetPassword(data.email).then(data => {
                                if (data['type'] == 1) {
                                    this.alertCtrl.create({
                                        title: 'Password Reset',
                                        message: 'You will receive an email with instructions on how to reset your password'
                                    }).present();
                                }
                            })
                        }
                    }
                }
            ]
        });
        resetPasswordPrompt.present();
    }

    onSignup() {
        this.navCtrl.push(SignupPage);
    }
    pushRegister() {
        this.navCtrl.push(SignupPage);
    }
    pushMain() {
        this.navCtrl.push(TabsPage);
    }
}
