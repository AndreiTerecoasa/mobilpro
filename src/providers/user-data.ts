import { Injectable } from '@angular/core';

import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { LoginPage } from '../login/login';

@Injectable()
export class UserData {
    url: string = 'https://mpvs.lse.org.ro/api/';
    _favorites = [];
    HAS_SEEN_TUTORIAL = 'hasSeenTutorial';

    constructor(
        public events: Events,
        public storage: Storage,
        public http: Http
        ) {}

    hasFavorite(sessionName) {
        return (this._favorites.indexOf(sessionName) > -1);
    };

    addFavorite(sessionName) {
        this._favorites.push(sessionName);
    };

    removeFavorite(sessionName) {
        let index = this._favorites.indexOf(sessionName);
        if (index > -1) {
            this._favorites.splice(index, 1);
        }
    };

    login(email, password) {
        var user = JSON.stringify({email: email, password: password});
        this.http.post('https://mpvs.lse.org.ro/api/user/login/', user).map(res => res.json()).subscribe(
            (data) => {
                data.loginType = 'normal';
                this.events.publish('user:loggedin', data);
            }
        );
    };
    loginFb(email, token) {
         var user = JSON.stringify({email: email, accesToken: token});
         this.http.post('https://mpvs.lse.org.ro/api/user/loginFb/', user).map(res => res.json()).subscribe(
            (data) => {
                console.log(JSON.stringify(data));
                if(data.type == 2) {
                    this.events.publish('user:nosuchuser');
                } else if(data.type == 1) {
                    data.loginType = 'fb';
                    this.events.publish('user:loggedin', data);
                }
            }
        );
    }
    setLoggedIn(data) {
        this.storage.set('authKey', data.key);
        this.storage.set('loginType', data.type);
        this.storage.set('userData', JSON.stringify(data.userData)).then(data => {
            console.log("User Data userData saved to storage");
        });
    };
    signup(signup) {
        signup.scope = 'attendant';
        signup.organisation = '-1';
        this.http.post('https://mpvs.lse.org.ro/api/user/', JSON.stringify(signup)).map(res => res.json()).subscribe(
            (data) => {
                console.log(JSON.stringify(data));
                if(data == "1") {
                    this.events.publish('user:registered', signup.email, signup.password);
                }
            }
        );
    };

    getUserData() {
        return this.storage.get('userData').then(userData => {
            return JSON.parse(userData);
        })
    }
    changeUserData() {

    }
    logout() {
        return this.unregisterNotifyable().then(value => {
            this.storage.remove('authKey');
            this.storage.remove('userData');
            this.events.publish('user:loggedout');
            return true;
        });
    };

    getAuthKey() {
        return this.storage.get('authKey').then((value) => {
            console.log("User Data getAuthKey: ", value);
            return value;
        });
    };

    // return a promise
    hasLoggedIn() {
        return this.storage.get('authKey').then((value) => {
            console.log("User Data hasLoggedIn:", value);
            return (value !== '' && value != null);
        });
    };

    checkHasSeenTutorial() {
        return this.storage.get(this.HAS_SEEN_TUTORIAL).then((value) => {
            return value;
        })
    };


    changePassword(password) {
        return new Promise(resolve => {
            this.storage.get('authKey').then(authKey => {
                this.http.post(this.url + '/user/changePassword/', JSON.stringify({authKey: authKey, newPassword: password})).map(res => res.json()).subscribe(data => {
                    resolve(data);
                })
            })
           
        })
        
    };

    registerNotifyable(deviceToken) {
       return new Promise(resolve => {
            this.storage.get('authKey').then(authKey => {
                if(authKey) {
                    var data = JSON.stringify({
                        authKey: authKey,
                        deviceToken: deviceToken
                    });
                    this.http.post(this.url + '/user/registerNotifyable/', data)
                        .map(res => res.json())
                        .subscribe(data => {
                            this.storage.set('isNotifyable', deviceToken);
                            resolve(data);
                        });
                }
            });                
       });
    }
    unregisterNotifyable() {
        return new Promise(resolve => {
            this.storage.get('authKey').then(authKey => {
                if(authKey) {
                    this.http.post(this.url + '/user/unregisterNotifyable/', JSON.stringify({authKey: authKey}))
                        .map(res => res.json())
                        .subscribe(data => {
                            if(data.type == 1) {
                                this.storage.set('isNotifyable', '').then(() => {
                                    resolve(data.message);
                                })
                            }
                        })
                } else {
                    console.log('no authKey', authKey);
                }
            })
        })
    }
    isRegisteredNotifyable() {
        return this.storage.get('isNotifyable').then(deviceToken => {
            return deviceToken !== '' && deviceToken != undefined && deviceToken != null;
        })
    }
    resetPassword(email) {
        return new Promise(resolve => {
            this.http.post(this.url + '/user/resetPassword/', JSON.stringify({email: email})).map( res => res.json()).subscribe(data => {
                resolve(data);
            })
        })
    }
    getNotifications() {
        return new Promise(resolve => {
            this.storage.get('authKey').then(authKey => {
                if(authKey) {
                    this.http.post(this.url + '/notification/', JSON.stringify({authKey: authKey, action: 'getAppNotifications'})).map(res => res.json()).subscribe(data => {
                        resolve(data);
                    })
                } else {
                    console.log('no authKey', authKey);
                }
            })
        })
    }
}
