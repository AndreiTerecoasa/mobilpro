import { Injectable } from '@angular/core';

import { Http } from '@angular/http';

import { UserData } from './user-data';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';


@Injectable()
export class AppsData {
    url: string = 'https://mpvs.lse.org.ro/api/'
    
    data: any;
    applications: any;
    tracks: any;
    schedule: any;


    mapData: any[] = [{
        "name": "Liga Studentilor Electronisti",
        "address": "Corp A, Cam 229, Bulevardul Iuliu Maniu 1-3, București",
        "lat": 44.44109,
        "lng": 26.051927,
        "center": true
    }, {
        "name": "Biblioteca Centrala UPB",
        "address": "Splaiul Independeței, 313, București 060042",
        "lat": 44.4410688,
        "lng": 26.0492465,
    }];

    constructor(
        public http: Http, 
        public user: UserData,
        public storage: Storage,
        ) { }

    

    loadAppsFromApi(): any {
         return new Promise(resolve => {
            this.http.get(this.url + 'app/')
                .map(res => res.json())
                .subscribe(data => {
                    this.applications = data.data;
                    this.storage.set('apps', JSON.stringify(data.data));
                    resolve(this.applications);
                });
            }); 
    }   
    loadTracksFromApi(): any {
        return new Promise(resolve => {
            this.http.get(this.url + 'track/').map(res => res.json())
                .subscribe(data => {
                    if(data.type) {
                        this.tracks = data.data;
                        this.storage.set('tracks', JSON.stringify(data.data));
                        resolve(this.tracks);    
                    }
                })
        })
    }
    loadScheduleFromApi(): any {
        return new Promise(resolve => {
            this.http.get(this.url + 'schedule/')
                .map(res => res.json())
                .subscribe(data => {
                    if(data.type == 1) {
                        this.schedule = data.sessions;
                        resolve(this.schedule);
                    }
                })
        })
    }
    loadTracks(): any {
        if(this.tracks) {
            console.log("tracks", this.tracks);
            return Promise.resolve(this.tracks);
        } else {
            //check if it's in storage
            this.storage.get('tracks').then(value => {
                if(value) {
                    console.log('tracks from storage');
                    this.tracks = JSON.parse(value);
                } else {
                    this.loadTracksFromApi();
                }
            })
        }


        
    }
    loadApps(): any {
        if (this.data) {
            return Promise.resolve(this.data);
        } else {
            this.storage.get('apps').then(value => {
                if(value) {
                    this.applications = JSON.parse(value);
                } else {
                    this.loadAppsFromApi();
                }
            })
        }     
    }
    loadSchedule(): any {
        if(this.schedule != null) {
            return Promise.resolve(this.schedule);
            
        } else {
            this.storage.get('schedule').then(value => {
                if(value) {
                    return new Promise(resolve => {
                        this.schedule = JSON.parse(value);
                        resolve(this.schedule);
                    })
                } else {
                    return new Promise(resolve => {
                        this.http.get(this.url + 'schedule/')
                            .map(res => res.json())
                            .subscribe(data => {
                                this.schedule = data;
                                this.storage.set('schedule', JSON.stringify(data));
                                resolve(this.schedule);
                            });
                    }); 
                }
            });
        } 
    } 
    appSendMessage(id, message) {
        return new Promise(resolve => {
            this.storage.get('authKey').then(authKey => {
                this.http.post(this.url + 'app/sendMessage/', JSON.stringify({id: id, message: message, authKey: authKey})).map(res => res.json()).subscribe(data => {
                    resolve(data);
                })
            })
        })
    }

    refreshData() {
        this.refreshApps();
        this.refreshSchedule();
        this.refreshTracks();
    }

    refreshApps() {
        return new Promise(resolve => {
            this.storage.remove('apps').then(value => {

                resolve(this.applications);
            })
        })
    }
    refreshTracks() {
        this.storage.remove('tracks').then(value => {
            this.loadTracks()
        })
    }
    refreshSchedule() {
        this.schedule = [];
        return new Promise(resolve => {
            this.storage.remove('schedule').then(value => {
                this.loadSchedule();
                resolve(this.schedule);
            })
        })
    }

    getApps() {
        return this.applications;
    }

    getTracks() {
        return this.tracks;
    }
    getSchedule() {
        return this.schedule;
    }
    getMap() {
       return this.mapData;
    }

}
