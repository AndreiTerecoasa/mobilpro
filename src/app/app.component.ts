import { Component, ViewChild, enableProdMode } from '@angular/core';
import { Events, MenuController, Nav, Platform, AlertController, ToastController } from 'ionic-angular';
import { Splashscreen, StatusBar, Push, Toast} from 'ionic-native';
import { Storage } from '@ionic/storage';
import { Deploy } from '@ionic/cloud-angular';
import { LoadingController } from 'ionic-angular';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { TabsPage } from '../pages/tabs/tabs';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { FeedbackPage } from '../pages/feedback/feedback';
import { AppContactPage } from '../pages/app-contact/app-contact';
import { NotificationsPage } from '../pages/notifications/notifications';
import { AppsData } from '../providers/apps-data';
import { UserData } from '../providers/user-data';
import { SupportersPage } from '../pages/supporters/supporters';

export interface PageInterface {
	title: string;
	component: any;
	icon: string;
	logsOut?: boolean;
	index?: number;
}

@Component({
	selector: 'main-wrapper',
	templateUrl: 'app.template.html',
})
export class ConferenceApp {
	// the root nav is a child of the root app component
	// @ViewChild(Nav) gets a reference to the app's root nav
	@ViewChild(Nav) nav: Nav;
	
	// List of pages that can be navigated to from the left menu
	// the left menu only works after login
	// the login page disables the left menu
	appPages: PageInterface[] = [
	{ title: 'Schedule', component: TabsPage, icon: 'calendar' },
	{ title: 'Applications', component: TabsPage, index: 1, icon: 'contacts' },
	{ title: 'Map', component: TabsPage, index: 2, icon: 'map' },
	{ title: 'About', component: TabsPage, index: 3, icon: 'information-circle' },
	{ title: 'Supporters', component: SupportersPage, index: 3, icon: 'briefcase' },
	
	];
	loggedInPages: PageInterface[] = [
	{ title: 'Account', component: AccountPage, icon: 'person' },
	{ title: 'Notifications', component: NotificationsPage, icon: 'notifications' },
	{ title: 'Feedback', component: FeedbackPage, icon: 'help' },
	];
	loggedOutPages: PageInterface[] = [
	{ title: 'Login', component: LoginPage, icon: 'log-in' },
	{ title: 'Feedback', component: FeedbackPage, icon: 'help' },
	{ title: 'Signup', component: SignupPage, icon: 'person-add' }
	];
	rootPage: any;
	push: any;
	
	doubleBackButtonExitBoolean: boolean = false;
	
	constructor(
	public events: Events,
	public userData: UserData,
	public menu: MenuController,
	public platform: Platform,
	public appsData: AppsData,
	public storage: Storage,
	public alertCtrl: AlertController,
	public toastCtrl: ToastController,
	public deploy: Deploy,
	public loadingCtrl: LoadingController
	) {
		this.platformReady();
		this.storage.get('hasSeenTutorial').then((hasSeenTutorial) => {
			if (hasSeenTutorial) {
				this.rootPage = TabsPage;
			} else {
				this.rootPage = TutorialPage;
			}
		});
		
		this.listenToLoginEvents();
		
		// load app data
		appsData.loadApps();
		appsData.loadTracks();
		appsData.loadSchedule();
		
		// decide which menu items should be hidden by current login status stored in local storage
		this.userData.hasLoggedIn().then((hasLoggedIn) => {
			console.log("Here maybe?", hasLoggedIn);
			this.enableMenu(hasLoggedIn === true);
		});
		
		
		
		this.userData.isRegisteredNotifyable().then(is => {
			if(is) {
				this.loggedInPages.push({
					title: 'Notifications', 
					component: NotificationsPage, 
					icon: 'notifications' 
				})
			}
			
		})
		
	}
	
	openPage(page: PageInterface) {
		// the nav component was found using @ViewChild(Nav)
		// reset the nav to remove previous pages and only have this page
		// we wouldn't want the back button to show in this scenario
		if (page.index) {
			this.nav.setRoot(page.component, { tabIndex: page.index });
			
		} else {
			this.nav.setRoot(page.component).catch(() => {
				console.log("Didn't set nav root");
			});
		}
		
		if (page.logsOut === true) {
			// Give the menu time to close before changing to logged out
			setTimeout(() => {
				this.userData.logout();
				this.enableMenu(false);
			}, 1000);
		}
	}
	openTutorial() {
		this.nav.setRoot(TutorialPage);
	}
	listenToLoginEvents() {
		this.events.subscribe('user:loggedin', () => {
			this.enableMenu(true);
		});
		
		// this.events.subscribe('user:registered', () => {
			// 	this.enableMenu(true);
			// });
			
			this.events.subscribe('user:loggedout', () => {
				this.enableMenu(false);
			});
			
			
			
			
		}
		
		enableMenu(loggedIn) {
			this.menu.enable(loggedIn, 'loggedInMenu');
			this.menu.enable(!loggedIn, 'loggedOutMenu');
		}
		
		platformReady() {
			// Call any initial plugins when ready
			this.platform.ready().then(() => {
				Splashscreen.hide();
				StatusBar.hide();
				
				
				
				
				
				
				this.push = Push.init({
					android: {
						senderID: "186613180055",
						icon: "notification",
						iconColor: "grey"
					},
					ios: {
						alert: "true",
						badge: false,
						sound: "true"
					},
					windows: {}
				});
				
				
				this.push.on('notification', (data) => {
					console.log('message', data.message);
					let self = this;
					//if user using app and push notification comes
					if (data.additionalData.foreground) {
						// if application open, show popup
						let confirmAlert = this.alertCtrl.create({
							title: 'New Notification',
							message: data.message,
							buttons: [{
								text: 'Ok',
								role: 'cancel'
							}]
						});
						confirmAlert.present();
					} else {
						//if user NOT using app and push notification comes
						//TODO: Your logic on click of push notification directly
						self.nav.push(NotificationsPage);
						console.log("Push notification clicked");
					}
				});
				this.push.on('unregister')
				this.push.on('error', (e) => {
					console.log(e.message);
				});
				
			});
			this.deploy.channel = 'dev';
			this.deploy.check().then((snapshotAvailable: boolean) => {
				let alertCltr = this.alertCtrl.create({
					message: "There is an update for your app. Do you want to download now the update?",
					title: "Update",
					buttons: [
					{
						text: "Yes. Download",
						role: "ok",
						handler: () => {
							let loadingctrl = this.loadingCtrl.create({
								content: "Downloading. Please wait"
							});
							loadingctrl.present();
							this.deploy.download().then(() => {
								this.deploy.extract().then(() => {
									loadingctrl.dismiss();
									this.deploy.load();
								})
							})
						}
					},
					{
						text: "No. Not now",
						role: "cancel",
						handler: () => {
							//nada
						}
					}
					]
				})
				
				if (snapshotAvailable) {
					// When snapshotAvailable is true, you can apply the snapshot
					alertCltr.present();
				}
			});
			
		}
		onMenuOpen(event) {
			this.events.publish('sidebar:open');
		}
		onMenuClose(event) {
			this.events.publish('sidebar:close');
		}
		
	}
	