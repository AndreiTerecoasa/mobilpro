import { NgModule } from '@angular/core';
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import { IonicApp, IonicModule } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { ConferenceApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { PopoverPage } from '../pages/about-popover/about-popover';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { MapPage } from '../pages/map/map';
import { SchedulePage } from '../pages/schedule/schedule';
import { ScheduleFilterPage } from '../pages/schedule-filter/schedule-filter';
import { SessionDetailPage } from '../pages/session-detail/session-detail';
import { SignupPage } from '../pages/signup/signup';
import { AppDetailPage } from '../pages/app-detail/app-detail';
import { AppsListPage } from '../pages/apps-list/apps-list';
import { TabsPage } from '../pages/tabs/tabs';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { FeedbackPage } from '../pages/feedback/feedback';
import { AppContactPage } from '../pages/app-contact/app-contact';
import { NotificationsPage } from '../pages/notifications/notifications';
import { SupportersPage } from '../pages/supporters/supporters';
import { AppsData } from '../providers/apps-data';
import { UserData } from '../providers/user-data';


const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '9f90fa06'
  }
};

@NgModule({
  declarations: [
    ConferenceApp,
    AboutPage,
    AccountPage,
    LoginPage,
    MapPage,
    PopoverPage,
    SchedulePage,
    ScheduleFilterPage,
    SessionDetailPage,
    SignupPage,
    AppDetailPage,
    AppsListPage,
    TabsPage,
    TutorialPage,
    FeedbackPage,
    AppContactPage,
    NotificationsPage,
    SupportersPage
  ],
  imports: [
    IonicModule.forRoot(ConferenceApp),
    CloudModule.forRoot(cloudSettings)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ConferenceApp,
    AboutPage,
    AccountPage,
    LoginPage,
    MapPage,
    PopoverPage,
    SchedulePage,
    ScheduleFilterPage,
    SessionDetailPage,
    SignupPage,
    AppDetailPage,
    AppsListPage,
    TabsPage,
    TutorialPage,
    FeedbackPage,
    AppContactPage,
    NotificationsPage,
    SupportersPage
  ],
  providers: [AppsData, UserData, Storage]
})
export class AppModule { }
